# Data Sine Walnut test
by Daniel Barrington  
📞 07491930897  
📧 daniel.barrington@gmail.com  

## Running
1. `npm i`
2. `npm start`

### Added dependancies
[Styled Components](https://www.styled-components.com/)

#### TODO
1. Displaying more than the top 10 tags
2. Writing tests 😬
3. Cleaning up CSS and general refinement of the styling  
I'm still to find the perfect balance of a global style system and scoped component styles. I really like Styled Components but also like the utility of Tailwind.css
4. Mobile layout
