import React from 'react'
import styled from 'styled-components'

const ImageContainer = styled.div`
    position: relative;
    flex: 2;
    padding: 10px;
    h1 {
        position: absolute;
        bottom: 10px;
        left: 20px;
        background-color: rgba(255, 255, 255, 0.5);
        padding: 10px 20px;
        transition: opacity 1s;
    }
    &:hover {
        h1 {
            opacity: 0;
        }
    }
`

const Img = styled.img`
    width: 100%;
`

const Image = ({ location, name }) => (
    <ImageContainer>
        <Img src={location} alt={name} />
        <h1>{name}</h1>
    </ImageContainer>
)

export default Image
