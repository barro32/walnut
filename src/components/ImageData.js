import React from 'react'
import styled from 'styled-components'

const DataContainer = styled.div`
    padding: 10px;
    flex: 1;
    > div {
        margin-bottom: 10px;
        padding: 5px 10px;
        background-color: #fff;
        display: flex;
        justify-content: space-between;
        > span:first-of-type {
            text-transform: capitalize;
        }
    }
`

const ImageData = ({ data, title }) => {
    const dataArray = Object.entries(data)
    return (
        <DataContainer>
            <h3>{title}</h3>
            {
                dataArray.map(obj => (
                    <div key={obj[1]}>
                        <span>{obj[0]}</span>
                        <span>{Math.floor(obj[1] * 100, 2)}%</span>
                    </div>
                ))
            }
        </DataContainer>

    )
}

export default ImageData
