import React, { useState } from 'react'
import styled from 'styled-components'

const ScoreContainer = styled.div`
    flex: 1;
    padding: 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
    h3 {
        text-align: left;
        width: 100%;
    }
`

const MaxScore = styled.div`
    background-color: #fff;
    border-radius: 100%;
    width: 10vw;
    height: 10vw;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0 0 0 5px lightblue;
`

const ActualScore = styled.div`
    background-color: lightblue;
    border-radius: 100%;
    width: ${props => props.score + '%'};
    height: ${props => props.score + '%'};
    display: flex;
    justify-content: center;
    align-items: center;
    font-weight: bold;
    transition: all 2s;
`

const Score = ({ score }) => {
    const percentScore = Math.floor(score * 100, 2)
    const [delayedScore, setDelayedScore] = useState(0)
    setTimeout(() => {
        setDelayedScore(percentScore)
    }, 500);
    return (
        <ScoreContainer>
            <h3>Score</h3>
            <MaxScore>
                <ActualScore score={delayedScore}>
                    {percentScore}%
                </ActualScore>
            </MaxScore>
        </ScoreContainer>
    )
}

export default Score
