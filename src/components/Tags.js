import React from 'react'
import styled from 'styled-components'

const TagsContainer = styled.div`
    flex: 1;
    padding: 10px;
`
const TagContainer = styled.span`
    display: inline-block;
    padding: 10px 15px;
    margin: 5px;
    border-radius: 2px;
    background-color: lightgreen;
`

const Tag = ({ name }) => (
    <TagContainer>{name}</TagContainer>
)

const Tags = ({ tags }) => {
    const sortedTags = tags.sort((tagA, tagB) => tagB.strength - tagA.strength)
    const top10 = sortedTags.slice(0, 10)
    return (
        <TagsContainer>
            <h3>Tags</h3>
            {top10.map(tag => <Tag name={tag.name} strenght={tag.strenght} key={tag.name} />)}
        </TagsContainer>
    )
}

export default Tags
