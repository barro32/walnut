import React from 'react'
import image from './image.json'
import styled from 'styled-components'
import Tags from './components/Tags'
import Score from './components/Score'
import Image from './components/Image'
import ImageData from './components/ImageData'

const Layout = styled.div`
  display: flex;
  width: 80vw;
  margin: auto;
  max-height: 100vh;
  padding: 20px;
  box-sizing: border-box;
  background-color: #eee;
  font-family: sans-serif;

  .main {
    flex: 2;
    display: flex;
    flex-direction: column;
    .flex {
      display: flex;
      flex: 1;
    }
  }
  .side {
    flex: 1;
    display: flex;
    flex-direction: column;
  }
`

const App = () => (
  <Layout>
    <div className="main">
      <div className="flex">
        <Tags tags={image.tags} />
        <Score score={image.score} />
      </div>
      <Image location={image.location} name={image.name} />
    </div>
    <div className="side">
      <ImageData data={image.composition} title="Composition" />
      <ImageData data={image.colour} title="Colour" />
      <ImageData data={image.texture} title="Texture" />
    </div>
  </Layout>
)

export default App
